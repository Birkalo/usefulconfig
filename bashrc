
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=3000
HISTFILESIZE=3000

# some aliases
alias ffs='sudo $(history -p !!)'
alias cd.='pushd ..'
alias cd..='pushd ../..'
alias cd...='pushd ../../..'
alias cd....='pushd ../../../..'
alias cd.....='pushd ../../../../..'
alias cd......='pushd ../../../../../..'
alias t='htop'
alias gti='git'
alias dc='popd'
alias dka='docker kill $(docker ps -q)'
alias dra='docker rm $(docker ps -a -q)'
alias dp='docker ps'
alias dpa='docker ps -a'
alias s='subl'
alias vbash='vim ~/.bashrc'
alias drka='docker rm -v $(docker ps --filter status=exited -aq); docker rmi $(docker images -q -f dangling=true)'
alias jq='jq -C'
alias cal='ncal -bM'
alias venv='pushd ~/py37venv; source ./bin/activate; popd'
alias stk='dirs -v'
alias less='bat --pager "less -r"'
alias nts='bat --pager "less -r" ~/Notes.txt'
alias c='clear'

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# smarter directory navigation
cd() {
    if [[ -z "$1" ]]; then
        pushd /home/toby
    else
        pushd "$1"
    fi
}

up() {
    if [[ -z "$1" ]]; then
        echo "Give a command"
    else
        $1 "!!*"
    fi
}

# cheatsheet shell commands
cheat(){
        curl cheat.sh/$1
}

#Permanent Global Variables
export LESS=R
export HISTTIMEFORMAT='%F %T '
export EDITOR=vim

get_branch()
{
  git branch --no-color 2>/dev/null | grep '^*' | awk '{print $2}'
}

is_branch_clean()
{
  git status 2>/dev/null | grep -q "working tree clean"
  return $?
}

show_branch()
{
  branch="$(get_branch)"
  if [[ -n "$branch" ]]
  then
    if is_branch_clean
    then
      echo "($branch)"
    else
      echo "($branch **)"
    fi
  fi
}

PROMPT_COMMAND='echo -ne "\033]0;${PWD##*/} - $(get_branch)\007"'
PS1='[\[\033[31m\]\W\[\033[32m\]$(show_branch)\[\033[0m\]]\$ '
